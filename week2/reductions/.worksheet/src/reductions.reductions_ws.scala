package reductions

import common._

object reductions_ws {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(103); 
  println("Welcome to the Scala worksheet");$skip(253); 
  
  
  def scanLeft[A](inp: Array[A],
               a0: A, f: (A,A) => A,
               out: Array[A]): Unit = {
    out(0) = a0
    var a = a0
    var i = 0
    while (i<inp.length) {
      a = f(a,inp(i))
      i = i + 1
      out(i) = a
    }
  };System.out.println("""scanLeft: [A](inp: Array[A], a0: A, f: (A, A) => A, out: Array[A])Unit""");$skip(43); 
  
  val in: Array[Int] = Array(1,2,3,4,5);System.out.println("""in  : Array[Int] = """ + $show(in ));$skip(48); 
  val out: Array[Int] = Array(0, 0, 0, 0, 0, 0);System.out.println("""out  : Array[Int] = """ + $show(out ));$skip(47); 
  
  scanLeft[Int](in, 100, (a,b) => a+b, out);$skip(6); val res$0 = 
  out;System.out.println("""res0: Array[Int] = """ + $show(res$0));$skip(132); 
  
  def mapSeq[A,B](lst: List[A], f: A => B): List[B] = lst match {
    case Nil => Nil
    case h :: t => f(h) :: mapSeq(t,f)
  };System.out.println("""mapSeq: [A, B](lst: List[A], f: A => B)List[B]""");$skip(204); 
  
  def mapASegSeq[A,B](inp: Array[A], left: Int, right: Int, f: A => B,
                      out: Array[B]): Unit = {
    var i=left
    while (i<right){
      out(i) = f(inp(i))
      i=i+1
    }
  };System.out.println("""mapASegSeq: [A, B](inp: Array[A], left: Int, right: Int, f: A => B, out: Array[B])Unit""");$skip(25); 
  
  val threshold = 10;System.out.println("""threshold  : Int = """ + $show(threshold ));$skip(360); ;
  
  def mapASegPar[A,B](inp: Array[A], left: Int, right: Int, f: A => B,
                      out: Array[B]): Unit = {
    if(right-left < threshold){
      mapASegSeq(inp,left,right,f,out)
    }
    else {
      val mid = left + (right-left)/2
      parallel(mapASegPar(inp, left, mid, f, out),
               mapASegPar(inp, mid, right, f, out))
    }
  };System.out.println("""mapASegPar: [A, B](inp: Array[A], left: Int, right: Int, f: A => B, out: Array[B])Unit""");$skip(397); 
  
  def reduceSeg[A](inp: Array[A], left: Int, right: Int, f: (A,A) => A): A = {
		if (right - left < threshold) {
			var res= inp(left);
			var i= left+1
			while (i < right) { res= f(res, inp(i)); i= i+1 }
				res
		} else {
			val mid = left + (right - left)/2
			val (a1,a2) = parallel(reduceSeg(inp, left, mid, f),
			                       reduceSeg(inp, mid, right, f))
			f(a1,a2)
		}
	};System.out.println("""reduceSeg: [A](inp: Array[A], left: Int, right: Int, f: (A, A) => A)A""");$skip(224); 
	
	
	
	def reduceSeg1[A](inp: Array[A], left: Int, right: Int,
		                a0: A, f: (A,A) => A): A = {
		var a= a0
		var i= left
		while (i < right) {
		  println("entro aqui")
			a= f(a, inp(i))
			i= i+1
		}
		a
	};System.out.println("""reduceSeg1: [A](inp: Array[A], left: Int, right: Int, a0: A, f: (A, A) => A)A""");$skip(201); 

	def mapSeg[A,B](inp: Array[A], left: Int, right: Int,
	                fi : (Int,A) => B, out: Array[B]): Unit = {
	  var i=left
    while (i<right){
      out(i) = fi(i,inp(i))
      i=i+1
    }
	};System.out.println("""mapSeg: [A, B](inp: Array[A], left: Int, right: Int, fi: (Int, A) => B, out: Array[B])Unit""");$skip(247); 

	def scanLeftPar[A](inp: Array[A], a0: A, f: (A,A) => A, out: Array[A]) = {
		val fi = { (i:Int,v:A) => reduceSeg1(inp, 0, i, a0, f) }
		mapSeg(inp, 0, inp.length, fi, out)
		val last = inp.length - 1
		out(last + 1) = f(out(last), inp(last))
	};System.out.println("""scanLeftPar: [A](inp: Array[A], a0: A, f: (A, A) => A, out: Array[A])Unit""");$skip(42); 
	
	val in2: Array[Int] = Array(2,4,5,6,7);System.out.println("""in2  : Array[Int] = """ + $show(in2 ));$skip(47); 
	scanLeftPar[Int](in2, 200, (a,b) => a+b, out);$skip(5); val res$1 = 
	out;System.out.println("""res1: Array[Int] = """ + $show(res$1))}
  
}
