package reductions

import scala.annotation._
import org.scalameter._
import common._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")
  }
}

object ParallelParenthesesBalancing {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {
    def checkOpenClose(chars: Array[Char], openCount: Int): Boolean = {
      if (chars.isEmpty) openCount == 0
      else if (openCount < 0) false
      else if (chars.head == '(') checkOpenClose(chars.tail, openCount + 1)
      else if (chars.head == ')') checkOpenClose(chars.tail, openCount - 1)
      else checkOpenClose(chars.tail, openCount)

    }

    checkOpenClose(chars, 0)
  }

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    def traverse(from: Int, until: Int, acc1: Int, acc2: Int): (Int, Int) = {
      def traverseAcc(iter: Int, acc1: Int, aac2: Int): (Int, Int) = {
        if (iter >= until) (acc1,acc2)
        else if (chars(iter) == '(') traverseAcc(iter+1, acc1+1, acc2)
        else if (chars(iter) == ')') {
          if(acc1>0) traverseAcc(iter+1, acc1-1, acc2)
          else traverseAcc(iter+1, acc1, acc2+1)
        }
        else traverseAcc(iter+1, acc1, acc2)
      }
      traverseAcc(from,acc1,acc2)
    }

    def reduce(from: Int, until: Int): (Int, Int) = {
      if(until - from <= threshold) traverse(from, until,0,0)
      else {
        val mid = from + (until - from) / 2
        val (p1, p2) = parallel(reduce(from, mid), reduce(mid, until))
        val matched = Math.min(p1._1, p2._2)
        (p1._1 + p2._1 - matched, p1._2 + p2._2 - matched)
      }
    }

    reduce(0, chars.length) == (0,0)
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
