package reductions

import common._

object reductions_ws {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  
  def scanLeft[A](inp: Array[A],
               a0: A, f: (A,A) => A,
               out: Array[A]): Unit = {
    out(0) = a0
    var a = a0
    var i = 0
    while (i<inp.length) {
      a = f(a,inp(i))
      i = i + 1
      out(i) = a
    }
  }                                               //> scanLeft: [A](inp: Array[A], a0: A, f: (A, A) => A, out: Array[A])Unit
  
  val in: Array[Int] = Array(1,2,3,4,5)           //> in  : Array[Int] = Array(1, 2, 3, 4, 5)
  val out: Array[Int] = Array(0, 0, 0, 0, 0, 0)   //> out  : Array[Int] = Array(0, 0, 0, 0, 0, 0)
  
  scanLeft[Int](in, 100, (a,b) => a+b, out)
  out                                             //> res0: Array[Int] = Array(100, 101, 103, 106, 110, 115)
  
  def mapSeq[A,B](lst: List[A], f: A => B): List[B] = lst match {
    case Nil => Nil
    case h :: t => f(h) :: mapSeq(t,f)
  }                                               //> mapSeq: [A, B](lst: List[A], f: A => B)List[B]
  
  def mapASegSeq[A,B](inp: Array[A], left: Int, right: Int, f: A => B,
                      out: Array[B]): Unit = {
    var i=left
    while (i<right){
      out(i) = f(inp(i))
      i=i+1
    }
  }                                               //> mapASegSeq: [A, B](inp: Array[A], left: Int, right: Int, f: A => B, out: Arr
                                                  //| ay[B])Unit
  
  val threshold = 10;                             //> threshold  : Int = 10
  
  def mapASegPar[A,B](inp: Array[A], left: Int, right: Int, f: A => B,
                      out: Array[B]): Unit = {
    if(right-left < threshold){
      mapASegSeq(inp,left,right,f,out)
    }
    else {
      val mid = left + (right-left)/2
      parallel(mapASegPar(inp, left, mid, f, out),
               mapASegPar(inp, mid, right, f, out))
    }
  }                                               //> mapASegPar: [A, B](inp: Array[A], left: Int, right: Int, f: A => B, out: Ar
                                                  //| ray[B])Unit
  
  def reduceSeg[A](inp: Array[A], left: Int, right: Int, f: (A,A) => A): A = {
		if (right - left < threshold) {
			var res= inp(left);
			var i= left+1
			while (i < right) { res= f(res, inp(i)); i= i+1 }
				res
		} else {
			val mid = left + (right - left)/2
			val (a1,a2) = parallel(reduceSeg(inp, left, mid, f),
			                       reduceSeg(inp, mid, right, f))
			f(a1,a2)
		}
	}                                         //> reduceSeg: [A](inp: Array[A], left: Int, right: Int, f: (A, A) => A)A
	
	
	
	def reduceSeg1[A](inp: Array[A], left: Int, right: Int,
		                a0: A, f: (A,A) => A): A = {
		var a= a0
		var i= left
		while (i < right) {
		  println("entro aqui")
			a= f(a, inp(i))
			i= i+1
		}
		a
	}                                         //> reduceSeg1: [A](inp: Array[A], left: Int, right: Int, a0: A, f: (A, A) => A
                                                  //| )A

	def mapSeg[A,B](inp: Array[A], left: Int, right: Int,
	                fi : (Int,A) => B, out: Array[B]): Unit = {
	  var i=left
    while (i<right){
      out(i) = fi(i,inp(i))
      i=i+1
    }
	}                                         //> mapSeg: [A, B](inp: Array[A], left: Int, right: Int, fi: (Int, A) => B, out
                                                  //| : Array[B])Unit

	def scanLeftPar[A](inp: Array[A], a0: A, f: (A,A) => A, out: Array[A]) = {
		val fi = { (i:Int,v:A) => reduceSeg1(inp, 0, i, a0, f) }
		mapSeg(inp, 0, inp.length, fi, out)
		val last = inp.length - 1
		out(last + 1) = f(out(last), inp(last))
	}                                         //> scanLeftPar: [A](inp: Array[A], a0: A, f: (A, A) => A, out: Array[A])Unit
	
	val in2: Array[Int] = Array(2,4,5,6,7)    //> in2  : Array[Int] = Array(2, 4, 5, 6, 7)
	scanLeftPar[Int](in2, 200, (a,b) => a+b, out)
                                                  //> entro aqui
                                                  //| entro aqui
                                                  //| entro aqui
                                                  //| entro aqui
                                                  //| entro aqui
                                                  //| entro aqui
                                                  //| entro aqui
                                                  //| entro aqui
                                                  //| entro aqui
                                                  //| entro aqui
	out                                       //> res1: Array[Int] = Array(200, 202, 206, 211, 217, 224)
  
}